import {Vue} from 'vue-property-decorator'
import * as directives from 'vuetify/lib/directives'

// For full framework
import Vuetify from 'vuetify/lib/framework'
import config from '~/configs'
import i18n from './vue-i18n'

/**
 * Vuetify Plugin
 * Main components library
 *
 * https://vuetifyjs.com/
 *
 */
Vue.use(Vuetify, {
    directives
})

const vuetify = new Vuetify({
    rtl: config.theme.isRTL,
    theme: {
        dark: config.theme.globalTheme === 'dark',
        options: {
            customProperties: true
        },
        themes: {
            dark: config.theme.dark,
            light: config.theme.light
        }
    },
    lang: {
        current: config.locales.locale,
        // To use Vuetify own translations without Vue-i18n comment the next line
        t: (key, ...params) => i18n.t(key, params)
    }
})

export default vuetify
