import _ from 'lodash'
import {Vue} from 'vue-property-decorator'
import {RouteConfig} from 'vue-router'
import {AdminPermission} from '~/data/permisson'
import store from '~/store'

function checkPermissions(required: string[] | string) {
    if (!required)
        return true

    const permission = store.getters['user/getPermission']

    //  If permission includes all means full access
    if (permission.includes(AdminPermission.All))
        return true

    const requiredList = _.isArray(required) ? required : [required]
    return _.difference(requiredList, permission).length === 0
}

function checkComponentPermission(route: RouteConfig) {
    return checkPermissions(route['component']!!['permission'])
}

const Permission = {
    install(Vue, options) {
        Vue.prototype.checkPermissions = checkPermissions
        Vue.prototype.checkComponentPermission = checkComponentPermission
    },
}
Vue.use(Permission)
