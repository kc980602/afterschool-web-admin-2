import firebase from 'firebase/app';
import 'firebase/auth';

import {Vue} from 'vue-property-decorator'

const config = JSON.parse(process.env.VUE_APP_FIREBASE_CONFIG || '')

firebase.initializeApp(config)

export function getCurrentUser() {
    return firebase.auth().currentUser
}

export async function getAccessToken(forceRenew: boolean = false) {
    return getCurrentUser()!.getIdToken(forceRenew)
}

export const auth = firebase.auth()

Vue.prototype.$firebase = firebase
