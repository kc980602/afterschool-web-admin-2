import browserDetect from "vue-browser-detect-plugin"
import {Vue} from 'vue-property-decorator'

/**
 * Simple plugin for Vue that detects browser name, version, and user-agent.
 * https://github.com/ICJIA/vue-browser-detect-plugin
 */
Vue.use(browserDetect)
