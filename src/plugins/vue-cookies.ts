import {Vue} from 'vue-property-decorator'
import VueCookies from 'vue-cookies'

/**
 * A simple Vue.js plugin for handling browser cookies
 * https://github.com/cmp-cc/vue-cookies
 */
Vue.use(VueCookies)
