import {Vue} from 'vue-property-decorator'
import VueShortkey from 'vue-shortkey'

/**
 * Easy keyboard shortcuts
 * https://github.com/iFgR/vue-shortkey
 */
Vue.use(VueShortkey)
