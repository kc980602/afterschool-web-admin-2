import moment from 'moment'
import {Vue} from 'vue-property-decorator'

/**
 * Date library momentjs
 * https://momentjs.com/
 */
Vue.prototype.$moment = moment
