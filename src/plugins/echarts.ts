import 'echarts/lib/chart/bar'
import 'echarts/lib/chart/line'
import 'echarts/lib/chart/pie'
import 'echarts/lib/chart/radar'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/polar'

/**
 * Vue ECharts
 * https://github.com/ecomfe/vue-echarts
 *
 */
import 'echarts/lib/component/tooltip'
import {Vue} from 'vue-property-decorator'
import VueECharts from 'vue-echarts'

Vue.component('e-charts', VueECharts)
