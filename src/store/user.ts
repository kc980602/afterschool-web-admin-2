import {Member} from '~/data/user'
import {toInstanceForce} from '~/utils/Serializer'

export const strict = false

const state = () => ({
    firebaseUser: null,
    isLogin: false,
    isClientLogin: false,
    member: null,
    permissions: []
})

const getters = {
    getUserId(state) {
        if (!!state.member)
            return state.member._id
    },
    getIsLogin(state) {
        return state.isLogin && !!state.member
    },
    getMember(state) {
        if (!!state.member)
            return state.member
        return null
    },
    getPermission: state => state.permissions
}
const mutations = {
    setFirebaseUser: (state, firebaseUser) => state.firebaseUser = firebaseUser,
    setIsLogin: (state, isLogin) => state.isLogin = isLogin,
    setMember: (state, member) => state.member = member,
    setPermissions: (state, permissions) => state.permissions = permissions,
}
const actions = {
    async saveFirebaseUser({commit, dispatch}, firebaseUser) {
        dispatch('fetchProfile', firebaseUser.uid)
        commit('setFirebaseUser', firebaseUser)
        commit('setIsLogin', !!firebaseUser)
    },
    fetchProfile({commit, dispatch}, uid) {
        return this._vm.$api.request(`${uid}/user-profile`, 'get').send(false)
            .then(res => {
                const member = toInstanceForce(new Member(), res.data)
                commit('setMember', member)
                return res.data
            }).catch(e => {
                return false
            })
    },
    fetchUserPermission({getters, commit}, memberId) {
        return this._vm.$api.request(`/member/${getters.getUserId || memberId}/permissions`, 'get').send()
            .then(res => {
                const permissions = res.data.permissions || []
                commit('setPermissions', permissions)
                return permissions
            }).catch(e => {
                return false
            })
    },
    signOut() {
        this._vm.$firebase.auth().signOut().then(() => {
            location.replace('/login')
        })
    }
}

const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

export default store
