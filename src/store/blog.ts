export const strict = false

export const state = () => ({
    blogTagList: []
})
export const getters = {
    getBlogTagList: state => state.blogTagList
}
export const mutations = {
    setBlogTagList: (state, blogTagList) => state.blogTagList = blogTagList
}
export const actions = {
    getBlogTagList({state, commit}) {
        //  get blog tag only if list empty
        if (state.blogTagList.length) {
            return true
        }

        // return this.app.$api(`/blog/tags`, 'get').send(false)
        //     .then((res) => {
        //         // commit('setBlogTagList', res.data.tags)
        //         return res.data.tags
        //     }).catch(e => {
        //         return false
        //     })
    },
}
