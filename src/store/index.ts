import {Vue} from 'vue-property-decorator'
import Vuex from 'vuex'
import BoardModule from '~/apps/board/store'
import EmailModule from '~/apps/email/store'
import TodoModule from '~/apps/todo/store'
import app from '~/store/app'
import user from '~/store/user'

Vue.use(Vuex)

export const strict = false

export const state = () => ({})
export const getters = {}
export const mutations = {}
export const actions = {}

const store = new Vuex.Store({
    modules: {
        app,
        user,
        'board-app': BoardModule,
        'email-app': EmailModule,
        'todo-app': TodoModule
    },
    strict: false
})

export default store
