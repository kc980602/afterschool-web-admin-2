import configs from '~/configs'
import Vuetify from '~/plugins/vuetify'

export const strict = false

const {product, theme} = configs
const {globalTheme} = theme

const state = () => ({
    product: product,
    globalTheme: globalTheme,
    /**
     *  Toast
     */
    toast: {
        show: false,
        color: 'black',
        message: '',
        timeout: 3000
    }
})

const getters = {}

const mutations = {
    /**
     * Theme
     */
    setGlobalTheme: (state, theme) => {
        Vuetify.framework.theme.dark = theme === 'dark'
        state.globalTheme = theme
    },
    /**
     *  Toast
     */
    setToast: (state, toast) => {
        const {color, timeout, message} = toast

        state.toast = {
            message,
            color,
            timeout,
            show: true
        }
    },
    hideToast: (state) => {
        state.toast.show = false
    },
    resetToast: (state) => {
        state.toast = {
            show: false,
            color: 'black',
            message: '',
            timeout: 3000
        }
    }
}

const actions = {
    /**
     * Theme
     */
    initGlobalTheme({commit}) {
        const cookieTheme = this._vm.$cookies.get('theme') || 'light'
        commit('setGlobalTheme', cookieTheme)
    },
    toggleGlobalTheme({state, commit}) {
        const theme = state.globalTheme === 'dark' ? 'light' : 'dark'
        Vuetify.framework.theme.dark = theme === 'dark'
        this._vm.$cookies.set('theme', theme, '10y')
        commit('setGlobalTheme', theme)
    },
    /**
     *  Toast
     */
    showToast({state, commit}, message) {
        if (state.toast.show) commit('hideToast')

        setTimeout(() => {
            commit('setToast', {
                color: 'black',
                message,
                timeout: 3000
            })
        })
    },
    showError({state, commit}, {message = 'Failed!', error}) {
        if (state.toast.show) commit('hideToast')

        setTimeout(() => {
            commit('showToast', {
                color: 'error',
                message: message + ' ' + error.message,
                timeout: 10000
            })
        })
    },
    showSuccess({state, commit}, message) {
        if (state.toast.show) commit('hideToast')

        setTimeout(() => {
            commit('showToast', {
                color: 'success',
                message,
                timeout: 3000
            })
        })
    }
}

const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}


export default store

