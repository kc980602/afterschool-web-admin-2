export default {
  'common': {
    'add': '加',
    'cancel': '取消',
    'description': '描述',
    'delete': '刪除',
    'title': '標題',
    'save': '救',
    'faq': '常問問題',
    'contact': '聯系我們',
    'tos': '服務條款',
    'policy': '隱私政策'
  },
  'board': {
    'titlePlaceholder': '輸入此卡的標題',
    'deleteDescription': '您確定要刪除此卡嗎？',
    'editCard': '編輯卡',
    'deleteCard': '刪除卡',
    'state': {
      'TODO': '去做',
      'INPROGRESS': '進行中',
      'TESTING': '測試中',
      'DONE': '完成'
    }
  },
  'chat': {
    'online': '在線用戶（{count}）',
    'addChannel': '添加頻道',
    'channel': '頻道|頻道',
    'message': '信息'
  },
  'email': {
    'compose': '編寫郵件',
    'send': '發送',
    'subject': '學科',
    'labels': '標簽',
    'emptyList': '空的電子郵件清單',
    'inbox': '收件箱',
    'sent': '已發送',
    'drafts': '草稿',
    'starred': '已加星標',
    'trash': '垃圾',
    'work': '工作',
    'invoice': '發票'
  },
  'todo': {
    'addTask': '添加任務',
    'tasks': '任務',
    'completed': '已完成',
    'labels': '標簽'
  },
  'dashboard': {
    'activity': '活動',
    'weeklySales': '每周銷售',
    'sales': '營業額',
    'recentOrders': '最近的訂單',
    'sources': '流量來源',
    'lastweek': '與上周',
    'orders': '訂單',
    'customers': '顧客',
    'tickets': '支持票',
    'viewReport': '查看報告'
  },
  'usermenu': {
    'profile': '個人資料',
    'signin': '登入',
    'dashboard': '儀表板',
    'signout': '登出'
  },
  'error': {
    'notfound': '找不到網頁',
    'other': '發生錯誤'
  },
  'check': {
    'title': '設置新密碼',
    'backtosign': '返回登錄',
    'newpassword': '新密碼',
    'button': '設置新密碼並登錄',
    'error': '動作鏈接無效',
    'verifylink': '正在驗証鏈接...',
    'verifyemail': '正在驗証電子郵件地址...',
    'emailverified': '電子郵件已驗証！重定向中...'
  },
  'forgot': {
    'title': '忘記密碼？',
    'subtitle': '輸入您的帳戶電子郵件地址，我們將向您發送一個鏈接以重置密碼。',
    'email': '電子郵件',
    'button': '要求重設密碼',
    'backtosign': '返回登錄'
  },
  'login': {
    'title': '登入',
    'email': '電子郵件',
    'password': '密碼',
    'button': '登入',
    'orsign': '或使用登錄',
    'forgot': '忘記密碼？',
    'noaccount': '還沒有帳號？',
    'create': '在此處創建一個',
    'error': '電子郵件/密碼組合無效'
  },
  'register': {
    'title': '創建帳號',
    'name': '全名',
    'email': '電子郵件',
    'password': '密碼',
    'button': '創建帳號',
    'orsign': '或注冊',
    'agree': '簽署即表示您同意',
    'account': '已經有帳號了？',
    'signin': '登入'
  },
  'utility': {
    'maintenance': '維護中'
  },
  'faq': {
    'call': '還有其他問題嗎？請伸出手'
  },
  'ecommerce': {
    'products': '產品展示',
    'filters': '篩選器',
    'collections': '館藏',
    'priceRange': '價格范圍',
    'customerReviews': '顧客評論',
    'up': '及以上',
    'brand': '牌',
    'search': '搜索產品',
    'results': '結果（{0}，共{1}）',
    'orders': '訂單',
    'shipping': '運輸',
    'freeShipping': '免費送貨',
    'inStock': '有現貨',
    'quantity': '數量',
    'addToCart': '添加到購物車',
    'buyNow': '立即購買',
    'price': '價錢',
    'about': '關於這個項目',
    'description': '描述',
    'reviews': '評論',
    'details': '產品詳情',
    'cart': '大車',
    'summary': '訂單摘要',
    'total': '總',
    'discount': '折扣',
    'subtotal': '小計',
    'continue': '繼續購物',
    'checkout': '退房'
  },
  'menu': {
    'search': '搜索（按“ Ctrl + /”進行聚焦）',
    'dashboard': '儀表板',
    'logout': '登出',
    'profile': '個人資料',
    'blank': '空白頁',
    'pages': '頁數',
    'others': '其他',
    'email': '電子郵件',
    'chat': '聊天室',
    'todo': '所有',
    'board': '任務板',
    'users': '用戶數',
    'usersList': '清單',
    'usersEdit': '編輯',
    'ecommerce': '電子商務',
    'ecommerceList': '產品展示',
    'ecommerceProductDetails': '產品詳情',
    'ecommerceOrders': '訂單',
    'ecommerceCart': '大車',
    'auth': '驗証頁面',
    'authLogin': '登錄/登錄',
    'authRegister': '注冊/注冊',
    'authVerify': '驗証郵件',
    'authForgot': '忘記密碼',
    'authReset': '重設密碼',
    'errorPages': '錯誤頁面',
    'errorNotFound': '找不到/ 404',
    'errorUnexpected': '意想不到的/ 500',
    'utilityPages': '實用頁面',
    'utilityMaintenance': '保養',
    'utilitySoon': '快來了',
    'utilityHelp': '常見問題/幫助',
    'levels': '菜單級別',
    'disabled': '菜單已禁用',
    'docs': '文獻資料',
    'feedback': '反饋',
    'support': '支持'
  },
  '$vuetify': {
    'badge': '徽章',
    'close': '關',
    'dataIterator': {
      'noResultsText': '沒有找到匹配的記錄',
      'loadingText': '正在載入項目...'
    },
    'dataTable': {
      'itemsPerPageText': '每頁行數：',
      'ariaLabel': {
        'sortDescending': '降序排列。',
        'sortAscending': '升序排列。',
        'sortNone': '未排序。',
        'activateNone': '激活以刪除排序。',
        'activateDescending': '激活以降序排列。',
        'activateAscending': '激活以升序排序。'
      },
      'sortBy': '排序方式'
    },
    'dataFooter': {
      'itemsPerPageText': '每頁項目：',
      'itemsPerPageAll': '所有',
      'nextPage': '下一頁',
      'prevPage': '上一頁',
      'firstPage': '第一頁',
      'lastPage': '最后一頁',
      'pageText': '{2}中的{0}-{1}'
    },
    'datePicker': {
      'itemsSelected': '已選擇{0}',
      'nextMonthAriaLabel': '下個月',
      'nextYearAriaLabel': '明年',
      'prevMonthAriaLabel': '前一個月',
      'prevYearAriaLabel': '前一年'
    },
    'noDataText': '沒有可用數據',
    'carousel': {
      'prev': '以前的視覺',
      'next': '下一個視覺',
      'ariaLabel': {
        'delimiter': '{1}的輪播幻燈片{0}'
      }
    },
    'calendar': {
      'moreEvents': '還有{0}個'
    },
    'fileInput': {
      'counter': '{0}個文件',
      'counterSize': '{0}個文件（共{1}個）'
    },
    'timePicker': {
      'am': 'AM',
      'pm': 'PM'
    },
    'pagination': {
      'ariaLabel': {
        'wrapper': '上午',
        'next': '下午',
        'previous': '分頁導航',
        'page': '下一頁',
        'currentPage': '上一頁'
      }
    }
  }
}
