import firebase from 'firebase/app'
import moment from 'moment'
import {NetworkRequestTypes} from './plugins/network-request'

declare module 'vue/types/vue' {
    interface Vue {
        $moment: moment.Moment
        $firebase: typeof firebase
        $api: NetworkRequestTypes

        checkPermissions: any
        checkComponentPermission: any
    }
}

