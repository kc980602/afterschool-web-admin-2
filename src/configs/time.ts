// time format for vue filter `| formatDate`
const time = {
    // https://momentjs.com/timezone/docs/#/using-timezones/
    zone: 'Asia/Hong_Kong',
    // https://momentjs.com/docs/#/displaying/format/
    format: 'LL'
}

export default time
