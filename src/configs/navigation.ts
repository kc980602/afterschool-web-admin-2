import menuApps from '~/configs/menus/apps.menu'
import menuPages from '~/configs/menus/pages.menu'
import menuUI from '~/configs/menus/ui.menu'

const navigation = {
    // main navigation - side menu
    menu: [
        {
            text: '',
            key: '',
            items: [
                {icon: 'mdi-view-dashboard-outline', key: 'menu.dashboard', text: 'Dashboard', link: '/dashboard'}
            ]
        },
        {
            text: 'Apps',
            items: menuApps
        },
        {
            text: 'UI - Theme Preview',
            items: menuUI
        },
        {
            text: 'Pages',
            key: 'menu.pages',
            items: menuPages
        },
        {
            text: 'Other',
            key: 'menu.others',
            items: [
                {icon: 'mdi-file-outline', key: 'menu.blank', text: 'Blank Page', link: '/blank'},
                {
                    key: 'menu.levels', text: 'Menu Levels',
                    items: [
                        {text: 'Menu Levels 2.1'},
                        {
                            text: 'Menu Levels 2.2',
                            items: [
                                {text: 'Menu Levels 3.1'},
                                {text: 'Menu Levels 3.2'}
                            ]
                        }
                    ]
                },
                {key: 'menu.disabled', text: 'Menu Disabled', disabled: true}
            ]
        }
    ]
}

export default navigation
