import currencies from '~/configs/currencies'
import icons from '~/configs/icons'
import locales from '~/configs/locales'
import navigation from '~/configs/navigation'
import theme from '~/configs/theme'
import time from '~/configs/time'
import toolbar from '~/configs/toolbar'

const configs = {
    // product display information
    product: {
        name: 'AfterSchool Admin',
        version: '1.2.0'
    },

    // time configs
    time,

    // icon libraries
    icons,

    // theme configs
    theme,

    // toolbar configs
    toolbar,

    // locales configs
    locales,

    // currencies configs
    currencies,

    // navigation configs
    navigation
}

export default configs
