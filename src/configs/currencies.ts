const currencies = {
    // current currency
    currency: {
        label: 'HKD',
        decimalDigits: 2,
        decimalSeparator: '.',
        thousandsSeparator: ',',
        currencySymbol: '$',
        currencySymbolNumberOfSpaces: 0,
        currencySymbolPosition: 'left'
    },

    // availabled currencies for user selection
    availableCurrencies: []
}
export default currencies
