import boardRoutes from '~/apps/board/routes'
import chatRoutes from '~/apps/chat/routes'
import emailRoutes from '~/apps/email/routes'
import todoRoutes from '~/apps/todo/routes'

const routes = [
    {
        path: '/apps/email',
        component: () => import(/* webpackChunkName: "apps-email" */ '~/apps/email/EmailApp.vue'),
        children: [
            ...emailRoutes
        ]
    },
    {
        path: '/apps/chat',
        component: () => import(/* webpackChunkName: "apps-chat" */ '~/apps/chat/ChatApp.vue'),
        children: [
            ...chatRoutes
        ]
    },
    {
        path: '/apps/todo',
        component: () => import(/* webpackChunkName: "apps-todo" */ '~/apps/todo/TodoApp.vue'),
        children: [
            ...todoRoutes
        ]
    },
    {
        path: '/apps/board',
        component: () => import(/* webpackChunkName: "apps-board" */ '~/apps/board/BoardApp.vue'),
        children: [
            ...boardRoutes
        ]
    }
]

export default routes
