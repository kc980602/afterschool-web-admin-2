import _ from 'lodash'
import {Vue} from 'vue-property-decorator'
import Router from 'vue-router'
import {getCurrentUser} from '~/plugins/firebase'

// Routes
import AppsRoutes from '~/router/apps.routes'
import EcommerceRoutes from '~/router/ecommerce.routes'
import PagesRoutes from '~/router/pages.routes'
import UIRoutes from '~/router/ui.routes'
import UsersRoutes from '~/router/users.routes'
import store from '~/store'

Vue.use(Router)

export const routes = [
    {
        path: '/',
        redirect: '/dashboard'
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "auth-signin" */ '~/pages/auth/SigninPage.vue'),
        meta: {
            layout: 'auth',
            noAuth: true
        }
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '~/pages/dashboard/DashboardPage.vue')
    },
    ...AppsRoutes,
    ...UIRoutes,
    ...PagesRoutes,
    ...UsersRoutes,
    ...EcommerceRoutes,
    {
        path: '/blank',
        name: 'blank',
        component: () => import(/* webpackChunkName: "blank" */ '~/pages/BlankPage.vue')
    },
    {
        path: '*',
        name: 'error',
        component: () => import(/* webpackChunkName: "error" */ '~/pages/error/NotFoundPage.vue'),
        meta: {
            layout: 'error'
        }
    }
]

export const routeDict = _.reduce(routes, (result, item) => {
    if (!item.name) {
        return result
    }
    return {
        ...result,
        [item.name]: item
    }
}, {})

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL || '/',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) return savedPosition

        return {x: 0, y: 0}
    },
    routes
})

/**
 * Before each route update
 */
router.beforeEach((to, from, next) => {
    const noAuth = to.matched.some((record) => record.meta.noAuth) || false
    const isLogin = (store.state as Object)['user'].isLogin
    if (!noAuth && !isLogin) {
        window.sessionStorage.setItem('after-login', to.fullPath)
        next(routeDict['login'].path)
    } else if (to.name!.startsWith('login') && isLogin) {
        next(routeDict['dashboard'].path)
    }

    return next()
})

/**
 * After each route update
 */
router.afterEach((to, from) => {
})

export default router
