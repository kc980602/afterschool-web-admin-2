import actions from './actions'
import mutations from './mutations'
import state from './state'

/*
|---------------------------------------------------------------------
| Email Vuex Store
|---------------------------------------------------------------------
*/
export default {
    namespaced: true,
    state,
    actions,
    mutations
}
