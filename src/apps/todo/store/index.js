import getters from './getters'
import mutations from './mutations'
import state from './state'

/*
|---------------------------------------------------------------------
| TODO Vuex Store
|---------------------------------------------------------------------
*/
export default {
    namespaced: true,
    state,
    getters,
    mutations
}
