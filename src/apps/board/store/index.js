import mutations from './mutations'
import state from './state'

/*
|---------------------------------------------------------------------
| Board Vuex Store
|---------------------------------------------------------------------
*/
export default {
    namespaced: true,
    state,
    mutations
}
