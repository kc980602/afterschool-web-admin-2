import 'animate.css/animate.min.css'
import {Vue} from 'vue-property-decorator'
import App from '~/App.vue'
import '~/assets/scss/theme.scss'
import '~/filters/capitalize'
import '~/filters/formatCurrency'
import '~/filters/formatDate'
import '~/filters/lowercase'
import '~/filters/uppercase'
import '~/plugins/animate'
import '~/plugins/apexcharts'
import '~/plugins/browser-detect'
import '~/plugins/clipboard'
import '~/plugins/echarts'
import '~/plugins/firebase'
import {auth} from '~/plugins/firebase'
import '~/plugins/moment'
import '~/plugins/network-request'
import '~/plugins/permission'
import '~/plugins/vue-cookies'
import '~/plugins/vue-head'
import i18n from '~/plugins/vue-i18n'
import '~/plugins/vue-shortkey'
import vuetify from '~/plugins/vuetify'
import router from '~/router'
import store from '~/store'

// Set this to false to prevent the production tip on Vue startup.
Vue.config.productionTip = false

/*
|---------------------------------------------------------------------
| Main Vue Instance
|---------------------------------------------------------------------
|
| Render the vue application on the <div id="app"></div> in index.html
|
| https://vuejs.org/v2/guide/instance.html
|
*/
let app: Vue
//  Delay create Vue instance to wait for firebase ready
auth.onAuthStateChanged(async user => {
    if (user && user.uid !== 'DUMMY') {
        store.dispatch('user/saveFirebaseUser', user)
    }

    if (!app) {
        app = new Vue({
            store,
            router,
            i18n,
            vuetify,
            render: (h) => h(App)
        }).$mount('#app')
        try {
            const loader = document.getElementsByClassName('loading-placeholder')[0]
            loader.remove()
        } catch (e) {
            console.log('Remove loader failed!')
        }
    }
})
