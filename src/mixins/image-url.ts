import {Component, Vue} from 'vue-property-decorator'


@Component
export default class ImageUrlMixin extends Vue {
  imageUrl(url, options = '') {
    const userAgent = this.$browserDetect.meta.name
    //  webview & safari does not support WebP
    //  Chrome + Mac OSX support WebP, ios Chrome desktop mode will change self browser name to Safari
    const ext = this.$browserDetect.isSafari || this.$browserDetect.isChromeIOS || this.$browserDetect.isIOS ? '.jpg' : '.webp'
    return url ? url + options + ext : null
  }

  avatarLink(id, options = '') {
    return id ? this.imageUrl(`${process.env.VUE_APP_WEB_DOMAIN}/images/profile-picture/${id}-propic`, options) : null
  }
}

