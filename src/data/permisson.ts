export enum AdminPermission {
    All = 'all',
    AdminPage = 'admin-page',
    Event = 'event',
    Coupon = 'coupon',
    Instructor = 'instructor',
    Invoice = 'invoice',
    DSERecources = 'dse-resources',
    Setting = 'setting',
    IP = 'ip',
    SendWhatsapp = 'send-whatsapp',
    ImportData = 'import-data',
}

export enum MemberPermission {
    ViewMemberData = 'view:member-data',
    SetMemberData = 'set:member-data',

    ViewMemberPermissions = 'view:member-permissions',
    SetMemberPermissions = 'set:member-permissions',

    SetRole = 'set:role',
    SetPassword = 'set:password',

    Verifications = 'member-verifications'
}

export enum ReportPermission {
    Dashboard = 'dashboard',
    TutorPerformance = 'tutor-dashboard',
    BusinessPerformance = 'business-dashboard',
    PaidLearnerReport = 'paid-learner-report',
    FreeLearnerReport = 'free-learner-report',
    SeriesReport = 'series-report',
}

export enum OrderPermission {
    Order = 'order',
    AbandonCart = 'abandon-cart',
    MakeupLesson = 'makeup-lesson',
}

export enum CoursePermission {
    ViewCourse = 'view:course',
    EditCourse = 'edit:course',
    CourseTag = 'course-tag',
    CourseSeries = 'course-series',
    StreamArchive = 'course-stream-archive',
    CourseReviews = 'course-reviews',
}

export enum BlogPermission {
    EditBlog = 'blog',
    EditBlogFooter = 'blog-footer',
    EditBlogFeatured = 'blog-featured',
}

export enum ShopPermission {
    EditShopItems = 'shop-items',
    ShopPurchaseRecords = 'shop-purchase-records',
}

export enum BannerPermission {
    WebBanner = 'web-banner',
    AppBanner = 'app-banner',
}

export enum ShortenLinkPermission {
    View = 'view:shorten-link',
    Create = 'create:shorten-link',
    Edit = 'edit:shorten-link',
}

export enum LogPermission {
    View = 'view:log',
}

export enum ProofFactorPermission {
    View = 'view:proof-factor',
    Create = 'create:proof-factor',
    Edit = 'edit:proof-factor',
}
