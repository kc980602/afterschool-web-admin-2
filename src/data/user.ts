export class Member {
    [key: string]: any;

    _id: string = ''
    member_id: string = ''
    email: string = ''
    phone: string = ''
    display_name: string = ''
    first_name: string = ''
    last_name: string = ''
    school: number = 0
    role: string = ''
    form: string = ''
    elective: number[] = []
    subjects: string[] = []
    status: string = ''
    title: number = -1
    dse: number = -1

    password_enabled: boolean = false;
    password: string = ''

    profile_picture_url = ''

    banned: boolean = false
    member_created_date: string = ''


    google_email: string = ''
    facebook_email: string = ''
    apple_email: string = ''
    google_id: string = ''
    facebook_id: string = ''
    apple_id: string = ''

    next_school_changeable = ''

    member_wallet = {}
    streak = {}
    cart = {}

    school_verification: VerificationStatus = VerificationStatus.NOT_SUBMITTED
    offline_paid: number = 0

    name: string = ''
    referral: boolean = false
}

export enum VerificationStatus {
    NOT_SUBMITTED = -2,
    REJECTED = -1,
    NOT_VERIFIED = 0,
    VERIFIED = 1
}
