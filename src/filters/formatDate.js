import {Vue} from 'vue-property-decorator'
import moment from 'moment-timezone'

Vue.filter('formatDate', (value, filterFormat) => {
  if (value) {
    return moment(value).format(filterFormat || 'DD/MM/YYYY HH:mm:ss' || 'lll')
  }

  return ''
})
