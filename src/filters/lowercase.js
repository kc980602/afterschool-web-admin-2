import {Vue} from 'vue-property-decorator'

Vue.filter('lowercase', (value) => {
  if (!value) return ''

  return value.toString().toLowerCase()
})
