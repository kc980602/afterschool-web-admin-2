import {Vue} from 'vue-property-decorator'

Vue.filter('uppercase', (value) => {
  if (!value) return ''

  return value.toString().toUpperCase()
})
